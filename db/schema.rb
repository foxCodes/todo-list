# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 20_240_205_025_232) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'accounts', force: :cascade do |t|
    t.string 'name'
    t.string 'surname'
    t.string 'patronymic'
    t.date 'birthday'
    t.string 'sex'
    t.string 'email', default: '', null: false
    t.string 'encrypted_password', default: '', null: false
    t.string 'reset_password_token'
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.integer 'sign_in_count', default: 0, null: false
    t.datetime 'current_sign_in_at'
    t.datetime 'last_sign_in_at'
    t.string 'current_sign_in_ip'
    t.string 'last_sign_in_ip'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['email'], name: 'index_accounts_on_email', unique: true
    t.index ['reset_password_token'], name: 'index_accounts_on_reset_password_token', unique: true
  end

  create_table 'accounts_roles', id: false, force: :cascade do |t|
    t.bigint 'account_id', null: false
    t.bigint 'role_id', null: false
    t.index %w[account_id role_id], name: 'index_accounts_roles_on_account_id_and_role_id'
  end

  create_table 'issues', force: :cascade do |t|
    t.string 'title'
    t.string 'discreption'
    t.string 'initiator'
    t.string 'phone_initiator'
    t.string 'place_initiator'
    t.string 'decision'
    t.boolean 'is_active'
    t.datetime 'date_limit'
    t.bigint 'mother_issue_id'
    t.bigint 'project_id'
    t.bigint 'sprint_id'
    t.bigint 'type_id'
    t.bigint 'kind_id'
    t.bigint 'relevance_id'
    t.bigint 'status_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['kind_id'], name: 'index_issues_on_kind_id'
    t.index ['mother_issue_id'], name: 'index_issues_on_mother_issue_id'
    t.index ['project_id'], name: 'index_issues_on_project_id'
    t.index ['relevance_id'], name: 'index_issues_on_relevance_id'
    t.index ['sprint_id'], name: 'index_issues_on_sprint_id'
    t.index ['status_id'], name: 'index_issues_on_status_id'
    t.index ['type_id'], name: 'index_issues_on_type_id'
  end

  create_table 'kinds', force: :cascade do |t|
    t.string 'name'
    t.string 'spec_name'
    t.string 'subscription'
    t.string 'icon'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'projects', force: :cascade do |t|
    t.string 'name'
    t.datetime 'begin'
    t.datetime 'end'
    t.string 'discreption'
    t.boolean 'is_active'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'relevances', force: :cascade do |t|
    t.string 'name'
    t.string 'spec_name'
    t.integer 'second_push'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'roles', force: :cascade do |t|
    t.string 'name'
    t.string 'spec_name'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'sprints', force: :cascade do |t|
    t.string 'name'
    t.datetime 'begin'
    t.datetime 'end'
    t.string 'discreption'
    t.boolean 'is_active'
    t.bigint 'project_id'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['project_id'], name: 'index_sprints_on_project_id'
  end

  create_table 'statuses', force: :cascade do |t|
    t.string 'name'
    t.string 'spec_name'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'types', force: :cascade do |t|
    t.string 'name'
    t.string 'spec_name'
    t.string 'subscription'
    t.string 'icon'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  add_foreign_key 'issues', 'issues', column: 'mother_issue_id'
  add_foreign_key 'issues', 'projects'
  add_foreign_key 'sprints', 'projects'
end
