# frozen_string_literal: true

Relevance.delete_all
Relevance.seed(:id,
               { id: 1, name: 'Срочно', spec_name: 'quickly', second_push: 28_800 },
               { id: 2, name: '1-2 дня', spec_name: 'two_days', second_push: 172_800 },
               { id: 3, name: '3-5 дней', spec_name: 'five_days', second_push: 432_000 },
               { id: 4, name: '7-10 дней', spec_name: 'ten_days', second_push: 864_000 },
               { id: 5, name: 'До 14 дней', spec_name: 'fourteen_days', second_push: 1_209_600 },
               { id: 6, name: 'До 30 дней', spec_name: 'thirty_days', second_push: 2_592_000 },
               { id: 7, name: 'Более 30 дней', spec_name: 'more_thirty_days', second_push: 3_888_000 })
