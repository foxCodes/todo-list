# frozen_string_literal: true

Kind.delete_all
Kind.seed(:id,
          { id: 1, name: 'Таск', spec_name: 'task', subscription: '', icon: 'mdi-star' },
          { id: 2, name: 'Баг', spec_name: 'bug', subscription: 'Ошибка уже в реализованом функционале',
            icon: 'mdi-bug' },
          { id: 3, name: 'Стори', spec_name: 'story', subscription: '', icon: 'mdi-tooltip-text' })

# mdi-flag
# mdi-tooltip-text
# mdi-lamp
# mdi-star-shooting
# mdi-bookmark-multiple
# mdi-message-reply-text-outline
# {id: 4, name: "", spec_name: "", subscription: "", icon: "mdi-tooltip-text"},
