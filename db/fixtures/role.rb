# frozen_string_literal: true

Role.delete_all
Role.seed(:id,
          { id: 1, name: 'Администратор', spec_name: 'administrator' },
          { id: 2, name: 'Супервизор', spec_name: 'supervisor' },
          { id: 3, name: 'Управляющий проектом', spec_name: 'manager_project' },
          { id: 4, name: 'Исполнитель', spec_name: 'executor' })
