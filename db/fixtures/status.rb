# frozen_string_literal: true

Status.delete_all
Status.seed(:id,
            { id: 1, name: 'Новая', spec_name: 'new' },
            { id: 2, name: 'В работе', spec_name: 'process' },
            { id: 3, name: 'Отложена', spec_name: 'postponed' },
            { id: 4, name: 'Выполнена', spec_name: 'done' })
