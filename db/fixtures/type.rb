# frozen_string_literal: true

Type.delete_all
Type.seed(:id,
          { id: 1, name: 'Базовая задача', spec_name: 'basic-task', subscription: '', icon: 'mdi-bookmark' },
          { id: 2, name: 'Подзадача', spec_name: 'sub-task', subscription: '', icon: 'mdi-bookmark-multiple' })
