# frozen_string_literal: true

class CreateStatuses < ActiveRecord::Migration[7.0]
  def self.up
    create_table :statuses do |t|
      t.string :name
      t.string :spec_name

      t.timestamps
    end
  end

  def self.down
    drop_table :statuses
  end
end
