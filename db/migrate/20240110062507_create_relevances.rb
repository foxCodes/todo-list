# frozen_string_literal: true

class CreateRelevances < ActiveRecord::Migration[7.0]
  def self.up
    create_table :relevances do |t|
      t.string :name
      t.string :spec_name
      t.integer :second_push

      t.timestamps
    end
  end

  def self.down
    drop_table :relevances
  end
end
