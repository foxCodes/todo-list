# frozen_string_literal: true

class CreateRoles < ActiveRecord::Migration[7.0]
  def self.up
    create_table :roles do |t|
      t.string :name
      t.string :spec_name

      t.timestamps
    end

    create_join_table :accounts, :roles do |t|
      t.index %i[account_id role_id]
    end
  end

  def self.down
    drop_join_table :accounts, :roles
    drop_table :roles
  end
end
