# frozen_string_literal: true

class CreateIssues < ActiveRecord::Migration[7.0]
  def self.up
    create_table :issues do |t|
      t.string :title
      t.string :discreption
      t.string :initiator
      t.string :phone_initiator
      t.string :place_initiator
      t.string :decision
      t.boolean :is_active
      t.datetime :date_limit

      t.references :mother_issue, foreign_key: { to_table: :issues }
      t.belongs_to :project, index: true, foreign_key: true
      t.belongs_to :sprint, index: true

      t.belongs_to :type
      t.belongs_to :kind

      t.belongs_to :relevance
      t.belongs_to :status

      t.timestamps
    end
  end

  def self.down
    drop_table :issues
  end
end
