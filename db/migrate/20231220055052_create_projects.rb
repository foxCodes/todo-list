# frozen_string_literal: true

class CreateProjects < ActiveRecord::Migration[7.0]
  def self.up
    create_table :projects do |t|
      t.string :name
      t.datetime :begin
      t.datetime :end
      t.string :discreption
      t.boolean :is_active

      t.timestamps
    end
  end

  def self.down
    drop_table :projects
  end
end
