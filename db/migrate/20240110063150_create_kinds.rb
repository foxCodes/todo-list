# frozen_string_literal: true

class CreateKinds < ActiveRecord::Migration[7.0]
  def self.up
    create_table :kinds do |t|
      t.string :name
      t.string :spec_name
      t.string :subscription
      t.string :icon

      t.timestamps
    end
  end

  def self.down
    drop_table :kinds
  end
end
