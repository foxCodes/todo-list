# frozen_string_literal: true

class CreateSprints < ActiveRecord::Migration[7.0]
  def self.up
    create_table :sprints do |t|
      t.string :name
      t.datetime :begin
      t.datetime :end
      t.string :discreption
      t.boolean :is_active

      t.belongs_to :project, index: true, foreign_key: true

      t.timestamps
    end
  end

  def self.down
    drop_table :sprints
  end
end
