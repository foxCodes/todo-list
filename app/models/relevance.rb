# frozen_string_literal: true

class Relevance < ApplicationRecord
  has_many :issues
end
