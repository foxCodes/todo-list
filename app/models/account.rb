# frozen_string_literal: true

class Account < ApplicationRecord
  has_and_belongs_to_many :roles
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :omniauthable
  devise :database_authenticatable,
         :recoverable,
         :rememberable,
         :validatable,
         :trackable

  validates :name, presence: true
  validates :surname, presence: true
  validates :patronymic, presence: true
  validates :birthday, presence: true
  validates :email, presence: true,
                    uniqueness: true,
                    format: { with: Devise.email_regexp }

  def self.gen_password(count_sym, count_nub, count_alf_b, count_alf_l)
    current_password = []
    count_sym.times do
      current_password << ['$', '#', '=', '+', '/', '*', '-', '@', '(', ')', '_', '>', '<'].to_a.sample
    end
    count_nub.times do
      current_password << (0..100).to_a.sample
    end
    count_alf_b.times do
      current_password << ('A'..'Z').to_a.sample
    end
    count_alf_l.times do
      current_password << ('a'..'z').to_a.sample
    end
    current_password.shuffle.join
  end
end
