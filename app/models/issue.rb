# frozen_string_literal: true

class Issue < ApplicationRecord
  has_many :child_issues, class_name: 'Issue', foreign_key: 'mother_issue'
  belongs_to :mother_issue, class_name: 'Issue', optional: true

  belongs_to :project
  belongs_to :sprint, optional: true
  belongs_to :type, optional: true
  belongs_to :kind, optional: true
  belongs_to :status, optional: true
  belongs_to :relevance, optional: true
end
