# frozen_string_literal: true

class Sprint < ApplicationRecord
  belongs_to :project
  has_many :issues, dependent: :destroy

  validates :name, presence: true
  validates :begin, presence: true
  validates :end, presence: true
  validates :discreption, presence: true
end
