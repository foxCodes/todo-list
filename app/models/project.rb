# frozen_string_literal: true

class Project < ApplicationRecord
  has_many :issues, dependent: :destroy
  has_many :sprints, dependent: :destroy

  validates :name, presence: true
  validates :discreption, presence: true
end
