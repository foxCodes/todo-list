# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :authenticate_account!

  def index; end

  def after_sign_in_path_for(_resource_or_scope)
    '/app#/issues'
  end

  #      def after_sign_in_path_for(resource)
  #        root_path
  #      end

  def current_acccout_date
    @current_account = {
      account: current_account.slice(:name, :surname, :patronymic, :email),
      roles: current_account.roles.map { |r| r[:spec_name] }
    }

    render json: @current_account.to_json
  end
end
