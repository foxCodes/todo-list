# frozen_string_literal: true

class SprintsController < ApplicationController
  before_action :authenticate_account!
  before_action :find_sprint, only: %i[show edit update destroy]

  def index
    @sprints = Sprint.all
    render json: @sprints.to_json
  end

  def index_by_project
    @sprints = Sprint.where(project_id: params[:project_id]) # write with SQL
    render json: @sprints.to_json
  end

  def new; end

  def create
    begin
      @sprint = Sprint.create(sprint_params)
      answer =  { status_error: !@sprint.errors.empty?, message_error: @sprint.errors.full_messages }
    rescue StandardError => e
      answer =  { status_error: true, message_error: [e.message] }
    end

    render json: answer.to_json
  end

  def show; end

  def edit; end

  def update
    begin
      @sprint.update(sprint_params)
      answer =  { status_error: !@sprint.errors.empty?, message_error: @sprint.errors.full_messages }
    rescue StandardError => e
      answer =  { status_error: true, message_error: [e.message] }
    end

    render json: answer.to_json
  end

  def destroy
    begin
      @sprint.destroy
      answer =  { status_error: !@sprint.errors.empty?, message_error: @sprint.errors.full_messages }
    rescue StandardError => e
      answer =  { status_error: true, message_error: [e.message] }
    end

    render json: answer.to_json
  end

  private

  def find_sprint
    @sprint = Sprint.find(params[:id])
  end

  def sprint_params
    params
      .require(:sprint)
      .permit(
        :name,
        :begin,
        :end,
        :discreption,
        :is_active,
        :project_id
      )
  end

  def check_access
    #     show_roles = ["administrator","manager"]
    #     answer = {error: true, message: "У Вас недостаточно прав"}
    #     if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
    #       render :json => answer.to_json
    #     end
  end
end
