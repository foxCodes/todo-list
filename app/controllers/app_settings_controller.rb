# frozen_string_literal: true

class AppSettingsController < ApplicationController
  before_action :authenticate_account!
  def index; end
end
