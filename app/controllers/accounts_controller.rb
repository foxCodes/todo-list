# frozen_string_literal: true

class AccountsController < ApplicationController
  before_action :authenticate_account!
  before_action :find_account, only: %i[update destroy]

  def index
    @accounts = Account.all.map do |account|
      {
        account:,
        role_ids: account.role_ids,
        roles_name: account.roles.collect(&:name).to_s.gsub(/"|\[|\]/, '')
      }
    end
    render json: @accounts.to_json
  end

  def role_index
    @roles = Role.all
    render json: @roles.to_json
  end

  def new; end

  def create
    begin
      @account = Account.create(account_params[:account][:data])
      @account.role_ids = account_params[:account][:role_ids] if @account.errors.empty?
      answer =  { status_error: !@account.errors.empty?, message_error: @account.errors.full_messages }
    rescue StandardError => e
      answer =  { status_error: true, message_error: [e.message] }
    end

    render json: answer.to_json
  end

  def show; end

  def edit; end

  def update
    begin
      @account.update(account_params[:account][:data])
      @account.role_ids = account_params[:account][:role_ids] if @account.errors.empty?
      answer =  { status_error: !@account.errors.empty?, message_error: @account.errors.full_messages }
    rescue StandardError => e
      answer =  { status_error: true, message_error: [e.message] }
    end

    render json: answer.to_json
  end

  def destroy
    begin
      raise StandardError.new "Удаление учетной записи администратора" if @account.role_ids.include?(1)
      raise StandardError.new "Суицид - ошибка, найди другой выход" if current_account.id == @account.id
      @account.destroy 
      answer =  { status_error: !@account.errors.empty?, message_error: @account.errors.full_messages }
    rescue StandardError => e
      answer =  { status_error: true, message_error: [e.message] }
    end

    render json: answer.to_json
  end

  def create_password
    begin
      answer =  { status_error: false, message_error: [] }
      answer[:data] = Account.gen_password(1, 2, 2, 3)
    rescue StandardError => e
      answer =  { status_error: true, message_error: [e.message] }
    end

    render json: answer.to_json
  end

  def check_login
    begin
      answer =  { status_error: false, message_error: [] }
      answer[:data] = Account.where(email: params[:login]).empty?
    rescue StandardError => e
      answer = { status_error: true, message_error: [e.message] }
    end

    render json: answer.to_json
  end

  private

  def find_account
    @account = Account.find(params[:id])
  end

  def account_params
    params
      .require(:account)
      .require(:data)

    params
      .require(:account)
      .require(:role_ids)

    params
      .permit(
        account: {
          data: %i[
            name
            surname
            patronymic
            birthday
            sex
            email
            password
          ],
          role_ids: []
        }
      )
  end

  def check_access
    #     show_roles = ["administrator","manager"]
    #     answer = {error: true, message: "У Вас недостаточно прав"}
    #     if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
    #       render :json => answer.to_json
    #     end
  end
end