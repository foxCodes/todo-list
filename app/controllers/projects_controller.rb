# frozen_string_literal: true

class ProjectsController < ApplicationController
  # skip_before_action :verify_authenticity_token
  before_action :authenticate_account!
  before_action :find_project, only: %i[show edit update destroy]

  def index
    @projects = Project.all
    render json: @projects.to_json
  end

  def new; end

  def create
    begin
      @project = Project.create(project_params)
      answer =  { status_error: !@project.errors.empty?, message_error: @project.errors.full_messages }
    rescue StandardError => e
      answer =  { status_error: true, message_error: [e.message] }
    end

    render json: answer.to_json
  end

  def show; end

  def edit; end

  def update
    begin
      @project.update(project_params)
      answer =  { status_error: !@project.errors.empty?, message_error: @project.errors.full_messages }
    rescue StandardError => e
      answer =  { status_error: true, message_error: [e.message] }
    end

    render json: answer.to_json
  end

  def destroy
    begin
      @project.destroy
      answer =  { status_error: !@project.errors.empty?, message_error: @project.errors.full_messages }
    rescue StandardError => e
      answer =  { status_error: true, message_error: [e.message] }
    end

    render json: answer.to_json
  end

  private

  def find_project
    @project = Project.find(params[:id])
  end

  def project_params
    params.required(:project).permit!

    params
      .require(:project)
      .permit(
        :name,
        :begin,
        :end,
        :discreption,
        :is_active
      )
  end

  def check_access
    #     show_roles = ["administrator","manager"]
    #     answer = {error: true, message: "У Вас недостаточно прав"}
    #     if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
    #       render :json => answer.to_json
    #     end
  end
end
