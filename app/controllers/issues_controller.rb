# frozen_string_literal: true

class IssuesController < ApplicationController
  before_action :authenticate_account!
  before_action :find_issue, only: %i[show edit update destroy]

  def index
    @issues = Issue.order(date_limit: :asc) # .where(name: text)
    render json: @issues.map { |issue|
                   {
                     issue:,
                     propertys: {
                       project_name: issue.project.name,
                       sprint_name: issue.sprint && issue.sprint.name,
                       type_name: issue.type.name,
                       type_icon: issue.type.icon,
                       kind_name: issue.kind.name,
                       kind_icon: issue.kind.icon,
                       relevance_name: issue.relevance.name,
                       status_name: issue.status
                     }
                   }
                 }
  end

  def new; end

  def create
    begin
      @issue = Issue.create(issue_params)
      answer =  { status_error: !@issue.errors.empty?, massege_error: @issue.errors }
    rescue StandardError => e
      answer =  { status_error: true, massege_error: [e.message] }
    end

    render json: answer.to_json
  end

  def show; end

  def edit; end

  def update
    begin
      @issue.update(issue_params)
      answer =  { status_error: !@issue.errors.empty?, massege_error: @issue.errors }
    rescue StandardError => e
      answer =  { status_error: true, massege_error: [e.message] }
    end

    render json: answer.to_json
  end

  def destroy
    begin
      @issue.destroy
      answer =  { status_error: !@issue.errors.empty?, massege_error: @issue.errors }
    rescue StandardError => e
      answer =  { status_error: true, massege_error: [e.message] }
    end

    render json: answer.to_json
  end

  private

  def find_issue
    @issue = Issue.find(params[:id])
  end

  def issue_params
    params.required(:issue).permit!
  end

  def check_access
    #     show_roles = ["administrator","manager"]
    #     answer = {error: true, message: "У Вас недостаточно прав"}
    #     if (show_roles & current_account.roles.collect(&:spec_name)).length == 0
    #       render :json => answer.to_json
    #     end
  end
end
