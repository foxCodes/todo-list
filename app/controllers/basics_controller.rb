# frozen_string_literal: true

class BasicsController < ApplicationController
  def index_type
    @types = Type.all.select(:id, :name, :spec_name, :subscription, :icon)
    render json: @types.to_json
  end

  def index_kind
    @kinds = Kind.all.select(:id, :name, :spec_name, :subscription, :icon)
    render json: @kinds.to_json
  end

  def index_status
    @statuses = Status.all.select(:id, :name, :spec_name)
    render json: @statuses.to_json
  end

  def index_relevance
    @relevances = Relevance.all.select(:id, :name, :spec_name, :second_push)
    render json: @relevances.to_json
  end
end
