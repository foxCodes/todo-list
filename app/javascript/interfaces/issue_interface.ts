interface IIssue {
    id?: number,
    title: string | null,
    discreption: string | null,
    initiator: string | null,
    phone_initiator: string | null,
    place_initiator: string | null,
    decision: string | null,
    is_active: boolean | null,
    date_limit: string,
    mother_issue_id: number | null,
    project_id: number | null,
    sprint_id: number | null,
    type_id: number | null,
    kind_id: number | null,
    relevance_id: number | null,
    status_id: number | null,
    created_at?: string,
    updated_at?: string
}

interface IPropertys {
    project_name: string,
    sprint_name: string | null,
    type_name: string,
    type_icon: string,
    kind_name: string,
    kind_icon: string,
    relevance_name: string,
    status_name: string,
}

interface IListIssue {
    issue: IIssue,
    propertys: IPropertys
}

export { IIssue, IListIssue };