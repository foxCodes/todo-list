interface IProject {
    id?: number,
    name: string | null,
    begin: Date | string | null,
    end: Date | string | null,
    discreption: string | null,
    is_active: boolean | null,
    created_at?: string,
    updated_at?: string
}

export { IProject };