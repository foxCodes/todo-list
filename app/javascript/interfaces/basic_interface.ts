interface IHeader {
    'Accept':  string,
    'Content-Type': string,
    'X-CSRF-TOKEN':  string
}

interface IRespond {
    status_error: boolean | null,
    message_error: string[] | any // дописать для объекта
}

interface IKind {
    id: number,
    name: string,
    spec_name: string,
    subscription: string,
    icon: string,
    created_at?: string,
    updated_at?: string
}

interface IType {
    id: number,
    name: string,
    spec_name: string,
    subscription: string,
    icon: string,
    created_at?: string,
    updated_at?: string
}

interface IStatus {
    id: number,
    name: string,
    spec_name: string,
    created_at?: string,
    updated_at?: string
}

interface IRelevance {
    id: number,
    name: string,
    spec_name: string,
    second_push: number
    created_at?: string,
    updated_at?: string
}

interface ISnackbar {
    is_visible: boolean,
    text: string,
    color: string,
    timeout: number,
    errors: string[]
}

export { IHeader, IRespond, IKind, IType, IStatus, IRelevance, ISnackbar };