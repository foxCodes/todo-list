interface IAccount {
    id?: number,
    name: string | null,
    surname: string | null,
    patronymic: string | null,
    birthday: string | null,
    sex: string | null,
    email: string | null,
    password?: string | null,
    created_at?: string,
    updated_at?: string

}

interface IAccounts {
    account: IAccount[],
    role_ids: number[],
    roles_name: string[],

}

interface ICurrentAccount {
    account: IAccount,
    roles: string[]
}


export { IAccount, ICurrentAccount, IAccounts };