interface ISprint {
    id?: number,
    name: string | null,
    begin: Date | string | null,
    end: Date | string | null,
    discreption: string | null,
    is_active: boolean | null,
    project_id: number,
    created_at?: string,
    updated_at?: string
}
export { ISprint };