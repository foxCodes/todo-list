// To see this message, add the following to the `<head>` section in your
// views/layouts/application.html.erb
//
//    <%= vite_client_tag %>
//    <%= vite_javascript_tag 'application' %>
console.log('Vite ⚡️ Rails')

// If using a TypeScript entrypoint file:
//     <%= vite_typescript_tag 'application' %>
//
// If you want to use .jsx or .tsx, add the extension:
//     <%= vite_javascript_tag 'application.jsx' %>

console.log('Visit the guide for more information: ', 'https://vite-ruby.netlify.app/guide/rails')

// Example: Load Rails libraries in Vite.
//
// import * as Turbo from '@hotwired/turbo'
// Turbo.start()
//
// import ActiveStorage from '@rails/activestorage'
// ActiveStorage.start()
//
// // Import all channels.
// const channels = import.meta.globEager('./**/*_channel.js')

// Example: Import a stylesheet in app/frontend/index.css
// import '~/index.css'

import { createApp } from "vue";
import { createPinia } from 'pinia';

import vuetify  from '../plugins/vuetify';
import router from '../plugins/routes';
import moment from 'moment'
import _ from 'lodash';

import Layout from "../components/basic_layout.vue";

moment.locale('ru');


document.addEventListener('DOMContentLoaded', () => {
    const pinia = createPinia();
    const app = createApp(Layout);

    app.use(pinia)
       .use(vuetify)
       .use(router)
       .use(_)
       .use(moment)
       .mount("#app");
});
