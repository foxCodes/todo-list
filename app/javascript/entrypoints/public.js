import { createApp } from 'vue/dist/vue.esm-bundler';
import vuetify  from '../plugins/vuetify';
import Layout from "../components/public/public_layout.vue";
import NewAccountSession from '../components/public/new_account_session.vue'


document.addEventListener('DOMContentLoaded', () => {
    const app = createApp({
        data() {
            return {
            }
        }
    });

    app.use(vuetify)
       .component('public-layout', Layout)
       .component('new-account-session', NewAccountSession)
       .mount("#app_public");
});
