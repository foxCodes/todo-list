import * as VueRouter from 'vue-router'

import Issue from '../components/issue.vue'
import Project from '../components/project.vue'
import Sprint from '../components/sprint.vue'
import Account from '../components/account.vue'
import AppSetting from '../components/app_setting.vue'


const routes = [
    { path: '/issues', component: Issue },
    { path: '/projects', component: Project },
    { path: '/sprints', component: Sprint },
    { path: '/accounts', component: Account },
    { path: '/settings', component: AppSetting },

]

export default VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes,
})

/*

import { createRouter, createWebHistory } from 'vue-router'
import Project from '../components/project.vue'

export default createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/projects', component: Project },
    ],
})
*/
