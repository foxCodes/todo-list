import moment from "moment";

    function getDateFormat(date: Date | string): string{//string|null
        return moment(date).format('DD.MM.YYYY');
    }

    function showSnackbar(color: string, text: string, object: any): void{ //any?
        object.color = color;
        object.text = text;
        object.is_visible = true;
    }

    function showRespondStatus(status_error: boolean,
                               errors: string[], 
                               message_success: string, 
                               message_error: string,
                               modification_object_flag: boolean,
                               clear_object_flag: boolean,
                               loadObjects: () => void,
                               clearObject: () => void, 
                               object: any
                               ): void{
        if(status_error){
            showSnackbar('rgba(255, 82, 82, 0.9)', message_error, object);
            if(errors.length > 0) object.timeout = "1e4"
            object.errors = [];
            object.errors = errors;
        }else{
            showSnackbar('rgba(85, 175, 80, 0.9)', message_success, object)
            if(modification_object_flag) loadObjects();
            if(clear_object_flag) clearObject();
        }

        //object.progress_line = false;
        //object.issueStore.changeProgressLine();
    }

export { getDateFormat, showSnackbar, showRespondStatus };