import { computed, Ref, ref, reactive } from 'vue';
import { defineStore } from 'pinia';
import {IHeader, IRespond}  from '../interfaces/basic_interface';
import {ISprint}  from '../interfaces/sprint_interface';

const hostname = window.location.origin;

export const useSprintStore = defineStore('sprint', () => {
    
    const sprint: ISprint = reactive({
        name: '',
        begin: null,
        end: null,
        discreption: '',
        is_active: false,
        project_id: null,
    });

    let answer: IRespond = reactive({
        status_error: null,
        message_error: [],
    });

    const sprints: Ref<ISprint[]> = ref([]);
    const is_active_sprint_dialog: Ref<boolean> = ref(false);
    
    const changeFlagDialog = (): void => {
        is_active_sprint_dialog.value = !is_active_sprint_dialog.value;
    };

    const clearSprint = (): void => {
        Object.assign(sprint, {
            name: '',
            begin: null,
            end: null,
            discreption: '',
            is_active: false,
            project_id: null,
        });
    }

    const createHeader = (token: string): IHeader => {
        return new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=UTF-8',
            'X-CSRF-TOKEN': token
        });
    }

    const loadSprints = async(): Promise<IRespond> => {
        try{
            const respond = await fetch(`${hostname}/sprints`);
            sprints.value = await respond.json(respond);
            answer.status_error = (respond.status==200 ? false : true);
            answer.message_error = [];

            return answer;
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    const loadSprintsByParams = async(id: number): Promise<IRespond> => {
        try{
            const respond = await fetch(`${hostname}/sprints_by_params?project_id=${id}`); // perhaps with id
            sprints.value = await respond.json(respond);
            answer.status_error = (respond.status==200 ? false : true);
            answer.message_error = [];

            return answer;
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    const createSprint = async(token: string): Promise<IRespond> => {
        try{
            const respond = await fetch(
                `${hostname}/sprints`,
                {
                    method: 'POST',
                    headers: createHeader(token),
                    body: JSON.stringify({sprint: sprint})
                });

            return await respond.json(respond);
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    const updateSprint = async(id: number, token: string, sprint: IRespond): Promise<IRespond> => {
        try{
            const respond = await fetch(
                `${hostname}/sprints/${id}`,
                {
                    method: 'PUT',
                    headers: createHeader(token),
                    body: JSON.stringify({sprint: sprint})
                });

            return await respond.json(respond);
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return await respond.json(respond);
        }
    }

    const destroySprint = async(id: number, token: string): Promise<IRespond> => {
        try{
            const respond = await fetch(
                `${hostname}/sprints/${id}`,
                {
                    method: 'DELETE',
                    headers: createHeader(token),
                });

            return await respond.json(respond);
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    return {
        is_active_sprint_dialog,
        sprint,
        sprints,
        changeFlagDialog,
        clearSprint,
        loadSprints,
        loadSprintsByParams,
        createSprint,
        updateSprint,
        destroySprint
    }
});
