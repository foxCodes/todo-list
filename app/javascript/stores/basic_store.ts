import { defineStore } from 'pinia';
import { IRespond }  from '../interfaces/basic_interface';
import { IKind, IType, IStatus, IRelevance} from "../interfaces/basic_interface";

const hostname = window.location.origin;

export const useBasicStore = defineStore('basic', {
    state: () => ({
        kinds: [] as IKind[],
        types: [] as IType[],
        statuses: [] as IStatus[],
        relevances: [] as IRelevance[],
        answer: {} as IRespond,
    }),
    getters: {
    },
    actions: {
        async loadKinds(): void{
            try{
                const respond = await fetch(`${hostname}/kinds`);
                this.kinds = await respond.json(respond);
                this.answer.status_error = (respond.status==200 ? false : true);
                //this.answer.massege_error = full_answer.massege_error;

                return this.answer;
            }catch(e){
                this.answer.status_error = false;
                this.answer.massege_error = e;

                return this.answer;
            }
        },
        async loadTypes(): void{
            try{
                const respond = await fetch(`${hostname}/types`);
                this.types = await respond.json(respond);
                this.answer.status_error = (respond.status==200 ? false : true);
                //this.answer.massege_error = full_answer.massege_error;

                return this.answer;
            }catch(e){
                this.answer.status_error = false;
                this.answer.massege_error = e;

                return this.answer;
            }
        },
        async loadStatuses(): void{
            try{
                const respond = await fetch(`${hostname}/statuses`);
                this.statuses = await respond.json(respond);
                this.answer.status_error = (respond.status==200 ? false : true);
                //this.answer.massege_error = full_answer.massege_error;

                return this.answer;
            }catch(e){
                this.answer.status_error = false;
                this.answer.massege_error = e;

                return this.answer;
            }
        },
        async loadRelevances(): void{
            try{
                const respond = await fetch(`${hostname}/relevances`);
                this.relevances = await respond.json(respond);
                this.answer.status_error = (respond.status==200 ? false : true);
                //this.answer.massege_error = full_answer.massege_error;

                return this.answer;
            }catch(e){
                this.answer.status_error = false;
                this.answer.massege_error = e;

                return this.answer;
            }
        },

    },
})
