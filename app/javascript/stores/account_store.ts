import { computed, Ref, ref, reactive } from 'vue';
import { defineStore } from 'pinia';
import {IHeader, IRespond} from '../interfaces/basic_interface';
import { IAccount, ICurrentAccount, IAccounts } from "../interfaces/account_interface";

const hostname = window.location.origin;

export const useAccountStore  = defineStore('account',() => {
    const account: IAccount = reactive({
        name: '',
        surname: '',
        patronymic: '',
        birthday: '',
        sex: null,
        email: null,
    }); 

    const current_account: ICurrentAccount = reactive({
        account: account,
        roles: []
    }); 

    let answer: IRespond = reactive({
        status_error: null,
        message_error: [],
    });

    // const arr1 = ref([] as number[])// equivalent to reactive({value:[]})
    // const arr = ref<number[]>([])// equivalent to reactive({value:[]})
    // const role_ids: number[] = reactive([]);
    // const role_ids: Ref<number[]> = ref([]);

    const accounts: Ref<IAccounts[]> = ref([]);
    const is_active_account_dialog: Ref<boolean> = ref(false);
    const sex_items: string[] = ['Мужской', 'Женский'];
    const roles: Ref<string[]> = ref([]);
    const password: Ref<string> = ref('');
    // const is_valid_login: boolean = false;
    
    const changeFlagDialog = (): void => {
        is_active_account_dialog.value = !is_active_account_dialog.value;
    }


    const createHeader = (token: string): IHeader => {
        return new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=UTF-8',
            'X-CSRF-TOKEN': token
        });
    }

    const genPassword = async(): Promise<IRespond> => {
        try{
            const respond = await fetch(`${hostname}/create_password`);
            let data = await respond.json(respond);
            password.value  = data?.data;
            answer.status_error = respond.status != 200;
            answer.message_error = [];

            return answer;
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    const loadRoles = async(): Promise<IRespond> => {
        try{
            const respond = await fetch(`${hostname}/roles`);
            roles.value = await respond.json(respond);
            answer.status_error = respond.status != 200;
            answer.message_error = [];

            return answer;
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }


    const loadAccounts = async(): Promise<IRespond> => {
        try{
            const respond = await fetch(`${hostname}/accounts`);
            accounts.value = await respond.json(respond);
            answer.status_error = respond.status != 200;
            answer.message_error = [];

            return answer;
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    const createAccount = async(token: string, role_ids: number[], account: IAccount): Promise<IRespond> => {
        try{
            if(password.value) account.password = password.value;

            const respond = await fetch(
                `${hostname}/accounts`,
                {
                    method: 'POST',
                    headers: createHeader(token),
                    body: JSON.stringify({account: {data: account, role_ids: role_ids}})
                }
            );

            return await respond.json(respond);
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    const updateAccount = async(id: number, token: string, role_ids: number[], account: IAccount): Promise<IRespond> => {
        try{
            const respond = await fetch(
                `${hostname}/accounts/${id}`,
                {
                    method: 'PUT',
                    headers: createHeader(token),
                    body: JSON.stringify({account: {data: account, role_ids: role_ids}})
                }
            );

            return await respond.json(respond);
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    const destroyAccount = async(id: number, token: string): Promise<IRespond> => {
        try{
            const respond = await fetch(
                `${hostname}/account/${id}`,
                {
                    method: 'DELETE',
                    headers: createHeader(token),
                }
            );

            return await respond.json(respond);
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

 
    const loadCurrentAccount = async(): Promise<IRespond> => {
        try{
            const respond = await fetch(`${hostname}/current_acccout_date`);
            // current_account = await respond.json(respond);
            Object.assign(current_account, await respond.json(respond));
            answer.status_error = respond.status != 200;
            answer.message_error = [];

            return answer;
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    } 

   /* 
    async checkLogin(): IRespond{
            try{
                const respond = await fetch(`${hostname}/check_login`);
                let data = await respond.json(respond);
                this.is_valid_login  = data?.data;
                this.answer.status_error = (respond.status==200 ? false : true);

                return this.answer;
            }catch(e){
                this.answer.status_error = false;
                this.answer.massege_error = e;

                return this.answer;
            }
        },

    
    */

    return {
        is_active_account_dialog,
        accounts,
        current_account,
        sex_items,
        roles,
        password,
        changeFlagDialog,
        genPassword,
        loadCurrentAccount,
        loadRoles,
        loadAccounts,
        createAccount,
        updateAccount,
        destroyAccount,
    }
});