import _ from 'lodash';
import { defineStore } from 'pinia';
import { IHeader, IRespond }  from '../interfaces/basic_interface';
import { IIssue }  from '../interfaces/issue_interface';

const hostname = window.location.origin;

export const useIssueStore = defineStore('issue', {
    state: () => ({
        issue: {
            title: '',
            discreption: '',
            initiator: '',
            phone_initiator: '',
            place_initiator: '',
            decision: '',
            is_active: true,
            mother_issue_id: null,
            project_id: null,
            sprint_id: null,
            type_id: null,
            kind_id: null,
            relevance_id: null,
            status_id: 1,
        } as IIssue,
        issues: [] as IIssue[],
        answer: {} as IRespond,
        issue_dialog: false as boolean,
        progress_line: false as boolean,
    }),
    getters: {
        newIssues(): IIssue[]{
            return this.issues.filter(issue=>issue.issue.status_id == 1)
        },

        anotherIssues(): IIssue[]{
            return this.issues.filter(issue=>issue.issue.status_id != 1)
        },
    },
    actions: {
        changeFlagDialog(): void{
            this.issue_dialog = !this.issue_dialog;
        },

        changeProgressLine(): void{
            this.progress_line = !this.progress_line;
        },

        clearIssue(){
            this.issue = _.cloneDeep({
                title: '',
                discreption: '',
                initiator: '',
                phone_initiator: '',
                place_initiator: '',
                decision: '',
                is_active: true,
                mother_issue_id: null,
                project_id: null,
                sprint_id: null,
                type_id: null,
                kind_id: null,
                relevance_id: null,
                status_id: null,
            });
        },

        createHeader(token: string): IHeader{
            const headers = new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'X-CSRF-TOKEN': token
            });

            return headers;
        },

       // async loadIssue(text: string): void{
        async loadIssue(): IRespond{
            try{
                //const respond = await fetch(`${hostname}/projects?text=${text}`);
                const respond = await fetch(`${hostname}/issues`);
                this.issues = await respond.json(respond);
                this.answer.status_error = (respond.status==200 ? false : true);

                return this.answer;
            }catch(e){
                this.answer.status_error = false;
                this.answer.massege_error = e;

                return this.answer;
            }
        },

        async createIssue(token: string): IRespond{
            try{
                const respond = await fetch(
                    `${hostname}/issues`,
                    {
                        method: 'POST',
                        headers: this.createHeader(token),
                        body: JSON.stringify({issue: this.issue})
                    });

                this.answer = await respond.json(respond);

                return this.answer;
            }catch(e){
                this.answer.status_error = false;
                this.answer.massege_error = e;

                return this.answer;
            }
        },

        async updateIssue(id: number, token: string): IRespond{
            try{
                const respond = await fetch(
                    `${hostname}/issues/${id}`,
                    {
                        method: 'PUT',
                        headers: this.createHeader(token),
                        body: JSON.stringify({issue: this.issue})
                    });

                this.answer = await respond.json(respond);

                return this.answer;
            }catch(e){
                this.answer.status_error = false;
                this.answer.massege_error = e;

                return this.answer;
            }
        },

        async destroyIssue(id: number, index: number, token: string): IRespond{
            try{
                const respond = await fetch(
                    `${hostname}/issues/${id}`,
                    {
                        method: 'DELETE',
                        headers: this.createHeader(token),
                    });

                this.answer = await respond.json(respond);
                //if(!this.answer.status_error) this.projects.splice(index,1);

                return this.answer;
            }catch(e){
                this.answer.status_error = false;
                this.answer.massege_error = e;

                return this.answer;
            }
        },
    },
})

