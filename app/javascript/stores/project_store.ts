import { computed, Ref, ref, reactive } from 'vue';
import { defineStore } from 'pinia';
import { IHeader, IRespond }  from '../interfaces/basic_interface';
import { IProject }  from '../interfaces/project_interface';


const hostname = window.location.origin;

export const useProjectStore = defineStore('project', () => {
    const project: IProject = reactive({
        name: '',
        begin: null,
        end: null,
        discreption: '',
        is_active: false,
    });

    let answer: IRespond = reactive({
        status_error: null,
        message_error: [],
    });

    const projects: Ref<IProject[]> = ref([]);
    // const projects: IProject[] = reactive([]);

    const project_dialog: Ref<boolean> = ref(false); //is_visible_mobile_submenu

    const changeFlagDialog = (): void => {
        project_dialog.value = !project_dialog.value;
    }

    const clearProject = (): void => {
        Object.assign(project, {
            name: '',
            begin: null,
            end: null,
            discreption: '',
            is_active: false,
        });
    }

    const createHeader = (token: string): IHeader => {
        const headers: IHeader = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=UTF-8',
            'X-CSRF-TOKEN': token
        });

        return headers;
    }

    const loadProjects = async(): Promise<IRespond> => {
        try{
            const respond = await fetch(`${hostname}/projects`);
            projects.value = await respond.json(respond);
            answer.status_error = (respond.status == 200 ? false : true);
            answer.message_error = [];
            //this.answer.message_error = full_answer.message_error;

            return answer;
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    const createProject = async(token: string): Promise<IRespond> => {
        try{
            const respond = await fetch(
                `${hostname}/projects`,
                {
                    method: 'POST',
                    headers: createHeader(token),
                    body: JSON.stringify({project: project})
                });

            //answer = await respond.json(respond);

            return await respond.json(respond);
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    const updateProject = async(id: number, token: string, project: IProject): Promise<IRespond> => {
        try{
            const respond = await fetch(
                `${hostname}/projects/${id}`,
                {
                    method: 'PUT',
                    headers: createHeader(token),
                    body: JSON.stringify({project: project})
                });
            // answer = await respond.json(respond);

            return  await respond.json(respond);
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    const destroyProject = async(id: number, token: string): Promise<IRespond> => {
        try{
            const respond = await fetch(
                `${hostname}/projects/${id}`,
                {
                    method: 'DELETE',
                    headers: createHeader(token),
                });

            // answer = await respond.json(respond);

            return  await respond.json(respond);
        }catch(e){
            answer.status_error = false;
            answer.message_error = e;

            return answer;
        }
    }

    return {
        project_dialog,
        project,
        projects,
        changeFlagDialog,
        clearProject,
        loadProjects,
        createProject,
        updateProject,
        destroyProject
    }
});