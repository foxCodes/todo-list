# toDo list [link](https://todo.amursu.ru) 

System for task management.

Version list:

* Ruby - 3.1.4
* Rails - 7.0.4
* PostgreSQL - 1.5
* Vite - 4.5.0
* Vue - 3.3.8
* Vuetify - 3.4.2