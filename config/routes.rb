# frozen_string_literal: true

Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get 'app', to: 'application#index'
  # root 'application#index'

  devise_for :accounts, skip: [:registrations]
  devise_scope :account do
    root to: 'devise/sessions#new'
  end

  get '/accounts' => 'accounts#index', :as => 'accounts_registration'
  post '/accounts' => 'accounts#create', :as => 'create_account_registration'
  put '/accounts/:id' => 'accounts#update', :as => 'update_account_registration'
  delete '/account/:id' => 'accounts#destroy', :as => 'destroy_account_registration'
  get '/create_password' => 'accounts#create_password', :as => 'password_account_registration'
  get 'current_acccout_date', controller: :application, action: :current_acccout_date
  get '/roles', controller: :accounts, action: :role_index
  get '/create_password', controller: :accounts, action: :create_password
  get '/check_login', controller: :accounts, action: :check_login

  resources :projects
  resources :issues
  resources :sprints

  get 'types', controller: :basics, action: :index_type
  get 'kinds', controller: :basics, action: :index_kind
  get 'statuses', controller: :basics, action: :index_status
  get 'relevances', controller: :basics, action: :index_relevance
  get 'sprints_by_params', controller: :sprints, action: :index_by_project
end
